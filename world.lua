local Class = require "hump/class"

require "settings"

local World = Class {}

function World:init(meter, width, height)
    self._world = love.physics.newWorld(0, 60*METER, true)
    self._body  = love.physics.newBody(self._world, 0, 0, "static")

    self._shapes = {
        love.physics.newChainShape("false", 0, 0, 0, height, width, height, width, 0),
        love.physics.newChainShape("false", 20, 250, 250, 250, 250, 260)
    }

    for _, s in ipairs(self._shapes) do
        local f = love.physics.newFixture(self._body, s, 1)
        f:setUserData({ type = "static" })
        f:setCategory(CATEGORY_STATIC)
    end
end

function World:setCallbacks(...)
    self._world:setCallbacks(...)
end

function World:update(dt)
    self._world:update(dt)
end

function World:draw()
    for _, s in ipairs(self._shapes) do
        love.graphics.polygon("line", self._body:getWorldPoints(s:getPoints()))
    end
end

return World
