local Game = {}

require "settings"
require "util"
require "hud"

local World = require "world"
local Char  = require "character"
local GS    = require "hump/gamestate"
local PauseMenu = require "pausemenu"

local char1_keymap
local char1
local char2
local char2_keymap

local function collideBullet(bullet, other)
    if other.type == "static" then
        bullet.self:destroy()
    elseif other.type == "char" then
        local char = other.self
        if char ~= bullet.owner then
            print"hit"
            char:getHit(20)
            bullet.self:destroy()
        end
    end
end

local function beginContact(a, b, coll)
    local aData = a:getUserData()
    local bData = b:getUserData()

    if (a == char1.fixtureBottom and bData.type == "static") or (b == char1.fixtureBottom and aData.type == "static") then
        char1.jumpEnabled = true
        char1:land()
    elseif (a == char2.fixtureBottom and bData.type == "static") or (b == char2.fixtureBottom and aData.type == "static") then
        char2.jumpEnabled = true
        char2:land()
    end

    if aData.type == "bullet" then
        collideBullet(aData, bData)
    elseif bData.type == "bullet" then
        collideBullet(bData, aData)
    end
end

local function endContact(a, b, coll)
    local aData = a:getUserData()
    local bData = b:getUserData()

    if (a == char1.fixtureBottom and bData.type == "static") or (b == char1.fixtureBottom and aData.type == "static") then
        char1.jumpEnabled = false
    elseif (a == char2.fixtureBottom and bData.type == "static") or (b == char2.fixtureBottom and aData.type == "static") then
        char2.jumpEnabled = false
    end
end

function Game.init()
    Game._world = World(METER, SCREEN_WIDTH, SCREEN_HEIGHT)
    Game._world:setCallbacks(beginContact, endContact)

    Game._bullets = {}

    local function addBullet(bullet)
        table.insert(Game._bullets, bullet)
    end

    char1_keymap = { "a", "d", "w", "e", "q" }
    char1 = Char(Game._world._world, 100, 100, Char.makeKeymap(unpack(char1_keymap)), love.graphics.newImage("sprites/SpriteSheet1.png"))
    char1.onMeleeAttack  = addBullet
    char1.onRangedAttack = addBullet

    char2_keymap = { "kp1", "kp3", "kp5", "kp4", "kp6" }
    char2 = Char(Game._world._world, 100, 100, Char.makeKeymap(unpack(char2_keymap)), love.graphics.newImage("sprites/SpriteSheet1.png"))
    char2.onMeleeAttack = addBullet
    char2.onMeleeAttack = addBullet
end

function Game:enter()
    print "game.enter"
end

function Game:update(dt)
    for i=#self._bullets, 1, -1 do
        local b = self._bullets[i]
        b:update(dt)
        if b.remove then
            table.remove(self._bullets, i)
        end
    end
    char1:update(dt)
    char2:update(dt)
    self._world:update(dt)
end

function Game:draw()
    self._world:draw()

    drawHealthBar(30, 30, char1.health)
    drawHealthBar(320, 30, char2.health)

    char1:draw()
    char2:draw()
    for _, b in ipairs(Game._bullets) do
        if not b.remove then b:draw() end
    end
end

function Game:focus()
    print "game.focus"
end

function Game:keypressed(key, unicode)
    if key == "escape" then
        GS.switch(PauseMenu.new(self))
        return
    end
    for _, k in ipairs(char1_keymap) do
        if key == k then char1:keypressed(key, unicode) end
    end
    for _, k in ipairs(char2_keymap) do
        if key == k then char2:keypressed(key, unicode) end
    end
end

function Game:keyreleased(key, unicode)
end

function Game:leave()
    print "game.leave"
end

function Game:quit()
    print "game.quit"
end

return Game
