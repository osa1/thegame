local Class  = require "hump/class"
local Bullet = require "bullet"
local anim8  = require "anim8"

local Char = Class {}

function Char.makeKeymap(moveLeft, moveRight, jump, meleeAttack, rangeAttack)
    return { moveRight = moveRight, moveLeft = moveLeft,
             jump = jump, meleeAttack = meleeAttack, rangeAttack = rangeAttack }
end

function Char:init(world, posX, posY, keyMap, spriteSheet, onAttack, onRangedAttack)
    self.keyMap = keyMap
    self.onAttack = onAttack
    self.onRangedAttack = onRangedAttack

    -- set physics stuff
    self.world   = world

    self.body        = love.physics.newBody(world, posX, posY, "dynamic")
    self.body:setFixedRotation(true)
    self.shapeTop    = love.physics.newCircleShape(0, -24, 10)
    self.shapeBottom = love.physics.newRectangleShape(0, 24, 10, 10, 0)
    self.shapeLeft   = love.physics.newCircleShape(-5, 0, 3)
    self.shapeRight  = love.physics.newCircleShape(5, 0, 3)

    self.fixtureTop    = love.physics.newFixture(self.body, self.shapeTop, 2)
    self.fixtureTop:setFriction(0)
    self.fixtureBottom = love.physics.newFixture(self.body, self.shapeBottom, 10)
    self.fixtureBottom:setFriction(5)
    self.fixtureBottom:setRestitution(0)
    self.fixtureLeft   = love.physics.newFixture(self.body, self.shapeLeft, 2)
    self.fixtureLeft:setFriction(0)
    self.fixtureRight  = love.physics.newFixture(self.body, self.shapeRight, 2)
    self.fixtureRight:setFriction(0)

    local ud = { type = "char", self = self }
    self.fixtureTop:setUserData(ud)
    self.fixtureBottom:setUserData(ud)
    self.fixtureLeft:setUserData(ud)
    self.fixtureRight:setUserData(ud)

    --

    self.jumpEnabled = false
    self.dirX        = 1
    self.health      = 100
    self.regen       = 15

    self.spriteSheet = spriteSheet
    self.animGrid = anim8.newGrid(60, 90, spriteSheet:getWidth(), spriteSheet:getHeight())
    self.anims = {
        stand       = anim8.newAnimation("loop", self.animGrid:getFrames(1,1, 2,1), 0.4, nil, false, false),
        walk        = anim8.newAnimation("loop", self.animGrid:getFrames(1,2, 2,2), 0.4, nil, false, false),
        meleeAttack = anim8.newAnimation("once", self.animGrid:getFrames(1,3, 2,3), 0.4, nil, false, false),
        rangeAttack = anim8.newAnimation("once", self.animGrid:getFrames(1,3, 2,3), 0.4, nil, false, false),
        jump        = anim8.newAnimation("once", self.animGrid:getFrames(1,4, 2,4), 0.4, nil, false, false),
        die         = anim8.newAnimation("once", self.animGrid:getFrames(1,5, 2,5), 0.4, nil, false, false)
    }

    self.lastAnim    = self.anims.stand
    self.currentAnim = self.anims.meleeAttack

    -- TODO: remove this states
    self.lastAnim_str = "stand"
    self.currentAnim_str = "meleeAttack"
end

function Char:update(dt)
    if self.health < 0 then
        self.health = 0
        self:_shiftAnims("die")
        self.currentAnim:update(dt)

        self.fixtureTop:destroy()
        self.fixtureBottom:destroy()
        self.fixtureLeft:destroy()
        self.fixtureRight:destroy()

        return
    end

    if self.health == 0 then return end

    self.health = self.health + self.regen * dt
    if self.health > 100 then self.health = 100 end

    local velX, velY = self.body:getLinearVelocity()

    local leftPressed  = love.keyboard.isDown(self.keyMap.moveLeft)
    local rightPressed = love.keyboard.isDown(self.keyMap.moveRight)
    local jumpPressed  = love.keyboard.isDown(self.keyMap.jump)

    -- update velocity and X direction
    local dir = 0
    if rightPressed then
        dir = 1
    elseif leftPressed then
        dir = -1
    end

    if dir ~= 0 then
        self.body:setLinearVelocity(dir * 400, velY)
        self.dirX = dir
    end

    local current = self.currentAnim
    local anims = self.anims
    current:update(dt)
    if current.status == "finished" and current.mode == "once" and not (self.health <= 0) then
    --if current.status == "finished" then
        self:_shiftAnims()
        self:_restartCurrentAnim()
    end
end

function Char:createMeleeBullet()
    local x, y = self.body:getWorldCenter()

    local bPosX, bPosY
    if self.dirX == 1 then
        bPosX = x + 20
    else
        bPosX = x - 20
    end
    bPosY = y

    return Bullet(self.world, self, 20, 70, bPosX, bPosY, 0.1, self.dirX * 100, 0)
end

function Char:createRangedBullet()
    local x, y = self.body:getWorldCenter()

    local bPosX, bPosY
    if self.dirX == 1 then
        bPosX = x + 40
    else
        bPosX = x - 40
    end
    bPosY = y

    return Bullet(self.world, self, 40, 20, bPosX, bPosY, 1, self.dirX * 500, 0, true)
end

function Char:keypressed(key, unicode)
    if self.health <= 0 then return end

    -- jump
    if key == self.keyMap.jump and self.jumpEnabled then
        velX, velY = self.body:getLinearVelocity()
        self.body:setLinearVelocity(velX, - 800)
        self:_setCurrentAnim("jump")
    end

    if key == self.keyMap.meleeAttack then
        if self.onMeleeAttack then self.onMeleeAttack(self:createMeleeBullet()) end
        self:_setCurrentAnim("meleeAttack")
    elseif key == self.keyMap.rangeAttack then
        if self.onRangedAttack then self.onRangedAttack(self:createRangedBullet()) end
        self:_setCurrentAnim("rangeAttack")
    end
end

function Char:keyreleased(key, unicode)

end

function Char:draw()
    local xPos, yPos = self.body:getPosition()

    if CHAR_DEBUG then
        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.circle("line", xPos, yPos-24, 10)
        love.graphics.polygon("line", self.body:getWorldPoints(self.shapeBottom:getPoints()))
        love.graphics.circle("line", xPos-5, yPos, 3)
        love.graphics.circle("line", xPos+5, yPos, 3)
    end

    love.graphics.rectangle("fill", xPos-30, yPos-45, 60, 90)
    self.currentAnim:draw(self.spriteSheet, xPos-30, yPos-45)
end

function Char:getHit(damage)
    if self.health > 0 then self.health = self.health - damage end
end

-- ---------------------------------------------------------
-- animation handling
-- ---------------------------------------------------------

function Char:_setCurrentAnim(anim)
    self:_shiftAnims(anim)
    self:_restartCurrentAnim()
end

function Char:_shiftAnims(anim)
    if not anim then
        self.lastAnim = self.anims.stand
        self.lastAnim_str = "stand"

        self.currentAnim = self.lastAnim
        self.currentAnim_str = self.lastAnim_str
    else
        self.lastAnim = self.currentAnim
        self.lastAnim_str = self.currentAnim_str

        self.currentAnim = self.anims[anim]
        self.currentAnim_str = anim
    end
end

function Char:_restartCurrentAnim()
    self.currentAnim = self.currentAnim:clone()
end

function Char:land()
    if self.currentAnim == self.anims.jump then
        self.currentAnim = self.anims.stand
    end
    self.lastAnim = self.anims.stand
end

return Char
