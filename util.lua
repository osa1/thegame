function isStatic(f)
    local userData = f:getUserData()
    return userData and userData.type == "static"
end
