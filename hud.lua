function drawHealthBar(posX, posY, health)
    love.graphics.rectangle("line", posX, posY, 140, 20)
    
    local r, g, b, a = love.graphics.getColor()
    love.graphics.setColor(255, 0, 0, 255)
    love.graphics.rectangle("fill", posX+5, posY+5, health * 130 / 100, 10)
    love.graphics.setColor(r, g, b, a)
end
