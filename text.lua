Text = {}
Text.__index = Text

function Text:new(text, font, color, posX, posY, vx, vy, fade, fadeSpeed)
    local o = {}
    setmetatable(o, Text)

    o.text  = text
    o.font  = font
    o.color = color
    o.posX  = posX - font:getWidth(text) / 2
    o.posY  = posY - font:getLineHeight(text) / 2
    o.vx    = vx
    o.vy    = vy
    o.fade  = fade
    o.fadeSpeed = fadeSpeed

    return o
end

function Text:draw()
    local r, g, b = love.graphics.getColor()

    love.graphics.setColor(self.color[1], self.color[2], self.color[3], self.fade)
    love.graphics.print(self.text, self.posX, self.posY)
    love.graphics.setColor(r, g, b)
end

function Text:update(dt)
    self.posX = self.posX + self.vx * dt
    self.posY = self.posY + self.vy * dt

    self.fade = self.fade - self.fadeSpeed * dt
    if self.fade < 0 then
        self.fade = 0
        return true
    end
    return false
end
