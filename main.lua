local GS    = require "hump/gamestate"
local Game  = require "game"

require "settings"

function love.load()
    love.graphics.setMode(SCREEN_WIDTH, SCREEN_HEIGHT, false, true) -- TODO: fsaa ?
    love.physics.setMeter(METER)

    --love.graphics.setFont(love.graphics.newFont("font.otf", 9))
    love.graphics.setFont(love.graphics.newImageFont("font.png", "abcdefghjklmnoprsuvyzwqxit"))
    GS.switch(Game)
end

function love.draw()
    --love.graphics.translate(15, 15)
    GS.draw()
    love.graphics.setCaption(tostring(love.timer.getFPS()))
end

function love.update(dt)
    GS.update(dt)
end

function love.keypressed(key, unicode)
    --if key == "escape" then
        --love.event.push("quit")
        --return
    --end
    GS.keypressed(key, unicode)
end

function love.keyreleased(key, unicode)
    GS.keyreleased(key, unicode)
end

function love.quit()
    GS.quit()
end
