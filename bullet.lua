local Class = require "hump/class"

require "settings"

local Bullet = Class {}

function Bullet:init(world, owner, width, height, posX, posY, dur, velX, velY, collideWStatic)
    self._owner = owner
    self._dur = dur

    self._body = love.physics.newBody(world, posX, posY, "dynamic")
    self._body:setFixedRotation(true)
    self._body:setLinearVelocity(velX, velY)
    self._body:setGravityScale(0)

    self._shape   = love.physics.newRectangleShape(width, height)
    self._fixture = love.physics.newFixture(self._body, self._shape, 1)
    self._fixture:setFriction(0)
    self._fixture:setUserData({ type = "bullet", owner = owner, self = self })
    if not collideWStatic then self._fixture:setMask(CATEGORY_STATIC) end

    self._remove = false
end

function Bullet:update(dt)
    self._dur = self._dur - dt
    if self._dur <= 0 and not self.remove then
        self:destroy()
    end
end

function Bullet:draw()
    if BULLET_DEBUG then
        love.graphics.polygon("line", self._body:getWorldPoints(self._shape:getPoints()))
    end
end

function Bullet:destroy()
    self._body:destroy()
    self.remove = true
end

return Bullet
