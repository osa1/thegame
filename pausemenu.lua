local GS    = require "hump/gamestate"
local Class = require "hump/class"

require "settings"

--local font = love.graphics.newImageFont("font.png", "abcdefghjklmnoprsuvyzqxit")
local font = love.graphics.newFont("visitor/visitor1.ttf", 20)

----------------------------------------------------------------------
-- MenuItem
----------------------------------------------------------------------

local MenuItem = Class {}

function MenuItem:init(text, onClick)
    self.text = text
    self.onClick = onClick

    self._delta     = -1
    self._alpha     = 255
    self._fadeSpeed = 0.3
    self._selected  = false

    self._width = font:getWidth(text)
end

function MenuItem:getWidth()
    return self._width
end

function MenuItem:getHeight()
    return font:getHeight()
end

function MenuItem:draw(x, y)
    --local x = x - font:getWidth(self.text) / 2
    --local y = y - font:getHeight() / 2

    love.graphics.setFont(font)
    local r, g, b, a = love.graphics.getColor()
    love.graphics.setColor(r, g, b, self._alpha)
    love.graphics.print(self.text, x, y)
    love.graphics.setColor(r, g, b, a)
end

function MenuItem:update(dt)
    if self._selected then
        self._alpha = self._alpha + self._delta * self._fadeSpeed * 1 / dt
        if self._alpha < 0 then
            self._alpha = 0
            self._delta = 1
        elseif self._alpha > 255 then
            self._alpha = 255
            self._delta = -1
        end
    end
end

function MenuItem:select()
    self._selected = true
end

function MenuItem:leave()
    self._selected = false
    self._alpha    = 255
    self._delta    = -1
end

function MenuItem:click()
    if self.onClick then self.onClick(self.text) end
end

function MenuItem:getHeight()
    return font:getHeight()
end

----------------------------------------------------------------------
-- PauseMenu
----------------------------------------------------------------------

local PauseMenu = {}
PauseMenu.__index = PauseMenu

local _linespace = 10

-- XXX: for some reason, using hump/class failing here because prev parameter
-- is read as nil however I call the init method(Class(...) or Class:init(...))
function PauseMenu.new(prev, linespace)
    assert(prev)

    local o = {}
    o._prev = prev
    o.linespace = linespace or _linespace

    o.items = {
        MenuItem("continue", function ()
            GS.switch(o._prev)
        end),
        MenuItem("restart", function ()
            --o._prev.init()
            GS.switch(o._prev)
        end),
        MenuItem("exit", function ()
            love.event.push("quit")
        end)
    }

    o._height = o.linespace * (#o.items - 1) + font:getHeight() * #o.items
    o._width  = 0
    for _, i in ipairs(o.items) do
        o._width = math.max(o._width, i:getWidth())
    end

    o._selected = 1
    o.items[1]:select()

    return setmetatable(o, PauseMenu)
end

function PauseMenu:getHeight()
    return self._height
end

function PauseMenu:getWidth()
    return self._width
end

function PauseMenu:update(dt)
    for _, m in ipairs(self.items) do
        m:update(dt)
    end
end

function PauseMenu:draw()
    self._prev:draw()
    local r, g, b, a = love.graphics.getColor()
    love.graphics.setColor(30, 40, 60, 230)
    love.graphics.rectangle("fill", 0, 0, 1000, 1000)
    love.graphics.setColor(r, g, b, a)

    local starty = (SCREEN_HEIGHT - self:getHeight()) / 2
    local textHeight = font:getHeight()

    for i, m in ipairs(self.items) do
        local startx = (SCREEN_WIDTH - m:getWidth()) / 2
        m:draw(startx, starty + textHeight * (i-1) + self.linespace * (i-1))
    end
end

function PauseMenu:_select(idx)
    self.items[self._selected]:leave()
    self._selected = idx
    self.items[idx]:select()
end

function PauseMenu:keypressed(key, unicode)
    if key == "escape" then
        GS.switch(self._prev)
    elseif key == "up" then
        if self._selected - 1 == 0 then
            self:_select(#self.items)
        else
            self:_select(self._selected - 1)
        end
    elseif key == "down" then
        self:_select(self._selected % #self.items + 1)
    elseif key == "return" then
        self.items[self._selected]:click()
    end
end

return PauseMenu
